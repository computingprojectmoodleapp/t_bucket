///IT11725602
///T. L. Wakwella

var win1 = Titanium.UI.currentWindow;		//enabling the current window to the tab

var lableSLIIT1 = Ti.UI.createLabel({		//creating a label
	text:"Sri Lanka Institute of Information Technology",
	color:"orange",
	font:{fontSize:20},
	height:Ti.UI.SIZE,
	width:Ti.UI.SIZE,
	top:10,
	textAlign:'center'
});

var viewMain = Titanium.UI.createView({		//creating a view control in the window
	height:100,
	width:"100%",
	top:15,
	left:0,
	backgroundColor:"white"
});

var lblLogin = Titanium.UI.createLabel({
	text:"You have not logged in.",
	top:5, 
	left:0,
	height:50,
	width:"100%",
	textAlign:"center",
	font:{fontSize:18},
	color:"black"
});

var btnLoginPg = Titanium.UI.createButton({			//creating a button
	title:"Log In",
	width:"25%",
	height:"40%",
	top:50,
	left:175,
	id:"btnLogin"
});

btnLoginPg.addEventListener("click",function(e){	//fires when btnLoginPg is clicked: hides the message and displays the login controls
	viewMain.visible=false;
	Ti.App.fireEvent("showView");
});

//adding the elements to the view control
viewMain.add(btnLoginPg);
viewMain.add(lblLogin);


var viewNews = Titanium.UI.createView({
	height:300,
	width:"100%",
	top:200,
	left:0,
});

var imgNews1 = Titanium.UI.createImageView({			//creating an ImageView
	image:"http://courseweb.sliit.lk/pluginfile.php/10443/block_html/content/PDP.png",
	height:"15%",
	width:"25%",
	top:10,
	left:180
});

var imgNews2 = Titanium.UI.createImageView({
	image:"http://courseweb.sliit.lk/pluginfile.php/10443/block_html/content/android.png",
	height:"10%",
	width:"25%",
	top:55,
	left:180
});

var imgNews3 = Titanium.UI.createImageView({
	image:"http://courseweb.sliit.lk/pluginfile.php/10443/block_html/content/Server%202012.png",
	height:"10%",
	width:"25%",
	top:90,
	left:180
});

var imgNews4 = Titanium.UI.createImageView({
	image:"http://courseweb.sliit.lk/pluginfile.php/10443/block_html/content/Linux.png",
	height:"10%",
	width:"25%",
	top:125,
	left:180
});

var imgNews5 = Titanium.UI.createImageView({
	image:"http://courseweb.sliit.lk/pluginfile.php/10443/block_html/content/CCNA%20Routing.png",
	height:"10%",
	width:"25%",
	top:160,
	left:180
});

var imgNews6 = Titanium.UI.createImageView({
	image:"http://courseweb.sliit.lk/pluginfile.php/10443/block_html/content/cisa.png",
	height:"10%",
	width:"25%",
	top:195,
	left:180
});

var imgNews7 = Titanium.UI.createImageView({
	image:"http://courseweb.sliit.lk/pluginfile.php/10443/block_html/content/Contact.png",
	height:"20%",
	width:"25%",
	top:245,
	left:180
});

var winWeb = Titanium.UI.createWindow({				//creating a new window to display the remote webpage
	title:"SLIIT",
	backgroundColor:"white",
});

var webView = Titanium.UI.createWebView({			//creating a WebView to access the remote link
	top:0,
	left:0
});

function WebOpen(){									//a function fired upon clicking the ImageViews
	alert("You are being redirected to http://www.sliit.lk");
	winWeb.add(webView);
	winWeb.open();
}

imgNews2.addEventListener("click",function(e){		//EventListeners to capture the click on ImageView
	//webView.release();
	webView.url="http://www.sliit.lk/index.php/professional-programs/mobile-application-development/mobile-application-development-for-android";
	WebOpen();
});

imgNews3.addEventListener("click",function(e){
	webView.url="http://www.sliit.lk/index.php/professional-programs/installing-and-configuring-windows-server-2012-program";
	WebOpen();
});

imgNews4.addEventListener("click",function(e){
	webView.url="http://www.sliit.lk/index.php/professional-programs/advanced-linux-administration";
	WebOpen();
});

imgNews5.addEventListener("click",function(e){
	webView.url="http://www.sliit.lk/index.php/professional-programs/cisco-networking-academy/ccna-new-curriculum/ccna-routing-switching";
	WebOpen();
	
});

imgNews6.addEventListener("click",function(e){
	webView.url="http://www.sliit.lk/index.php/professional-programs/information-systems-auditing/certified-information-systems-auditor-review-programme";
	WebOpen();
});

var lblContact = Titanium.UI.createLabel({
	text:"Contact:",
	height:20,
	width:"100%",
	top:225,
	left:180,
	font:{fontSize:12,fontStyle:"italic"},
	color:"black"
});

viewNews.add(imgNews1);
viewNews.add(imgNews2);
viewNews.add(imgNews3);
viewNews.add(imgNews4);
viewNews.add(imgNews5);
viewNews.add(imgNews6);
viewNews.add(lblContact);
viewNews.add(imgNews7);

//view control for public notices
var viewNotice = Titanium.UI.createView({
	height:200,
	width:"100%",
	top:425,
	left:0
});

var lblTitle = Titanium.UI.createLabel({
	text:"Title is shown here.",
	height:25,
	width:"100%",
	bottom:70,
	left:0,
	textAlign:"center",
	font:{fontSize:12,fontStyle:"italic"},
	color:"black"
});

var lblNotice = Titanium.UI.createLabel({
	text:"Notice goes here",
	height:100,
	width:"75%",
	bottom:0,
	left:75,
	textAlign:"left",
	color:"black",
	font:{fontSize:12}
});

//GET request method for pulling notices from the database
var viewData = Titanium.Network.createHTTPClient({
	onload:function(e){								//function called when response data are available
		var json = this.responseText;
		var response = JSON.parse(json);
		lblTitle.text=response.title;
		lblNotice.text=response.notice;
	}
});

//Preparing the connection
viewData.open("GET","http://10.0.2.2/notices.php");

//Sending the request
viewData.send();

viewNotice.add(lblTitle);
viewNotice.add(lblNotice);

//Adding the elements to Window
win1.add(viewMain);
win1.add(viewNews);
win1.add(viewNotice);

win1.add(lableSLIIT1);


