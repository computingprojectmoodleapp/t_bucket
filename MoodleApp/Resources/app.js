/*var windowLogin = Titanium.UI.createWindow({
	title:"Login",
	backgroundColor:"white",
	url:"LoginWindow.js"
});

windowLogin.open();
*/

var tabGroup = Titanium.UI.createTabGroup();
	
var windowHome = Titanium.UI.createWindow({
	title:"Home",
	backgroundColor:"white",
	url:"HomeWindow.js"
});

var tabHome = Titanium.UI.createTab({
	title:"Home",
	window:windowHome	
});

var lableSLIIT1 = Ti.UI.createLabel({
	text:"Sri Lanka Institute of Information Technology",
	color:"orange",
	font:{fontSize:20},
	height:Ti.UI.SIZE,
	width:Ti.UI.SIZE,
	top:10,
	//left : myLeft,
	textAlign:'center'
});

windowHome.add(lableSLIIT1);

var viewLogin = Titanium.UI.createView({
	height:200,
	width:"auto",
	top:50,
	backgroundColor:"white"
});

var lableUserName = Ti.UI.createLabel({
	text:"Username",
	color:'black',
	font:{fontSize:18},
	height:Ti.UI.SIZE,
	width:Ti.UI.SIZE,
	top:15,
	left:10,
	textAlign:'left'
});

var textfieldUserName = Titanium.UI.createTextField({
	top:10,
	heigh:"10%",	//20,
	left:120,
	width:"40%",	//150,
	borderStyle:Titanium.UI.INPUT_BORDERSTYLE_ROUNDED,
	hintText:"IT Number"
});

var lablePassword = Titanium.UI.createLabel({
	text:"Password",
	color:'black',
	font:{fontSize:18},
	height:Ti.UI.SIZE,
	width:Ti.UI.SIZE,
	top:65,
	left:10,
	textAlign:'left'
});

var textfieldPassword = Titanium.UI.createTextField({
	top:60,
	heigh:"10%",	//20,
	left:120,
	width:"40%",	//150,
	borderStyle:Titanium.UI.INPUT_BORDERSTYLE_ROUNDED,
	hintText:"Password"
});

var buttonLogin = Titanium.UI.createButton({
	title:"Login",
	width:"25%",
	height:"20%",
	top:110,
	left:10,
});

viewLogin.add(lableUserName);

viewLogin.add(textfieldUserName);

viewLogin.add(lablePassword);

viewLogin.add(textfieldPassword);

viewLogin.add(buttonLogin);

///IT11725602
viewLogin.visible=false;
windowHome.add(viewLogin);

Ti.App.addEventListener("showView",function(e){
	viewLogin.visible=true;
});
///IT11725602

var windowProfile = Titanium.UI.createWindow({
	title:"Profile",
	backgroundColor:"white",
	//tabBarHidden:false,
	url:"ProfileWindow.js"
});

var tabProfile = Titanium.UI.createTab({
	title:"Profile",
	touchEnabled:false,
	window:windowProfile
});

var windowCourse = Titanium.UI.createWindow({
	title:"Course",
	backgroundColor:"white",
	url:"CourseWindow.js"
});

var tabCourse = Titanium.UI.createTab({
	title:"Course",
	//active:false,
	window:windowCourse
});

tabGroup.addTab(tabHome);

tabGroup.addTab(tabProfile);

tabGroup.addTab(tabCourse);

tabGroup.open();


//tabGroup[1]. = false;

//windowProfile.hide();


var loginReq = Titanium.Network.createHTTPClient();
loginReq.onload = function()
{
	var json = this.responseText;
	var response = JSON.parse(json);
	if (response.logged == true)
	{
		alert("You have successfully logged in!");
	}
	else
	{
		alert(response.message);
	}
};

loginReq.onerror = function()
{
	alert("Network error");
};

/*
* Login Button Click Event
*/

buttonLogin.addEventListener('click',function(e)
{
	if (textfieldUserName.value != '' && textfieldPassword.value != '')
	{
		loginReq.open("POST","http://10.0.2.2:8888/post_auth.php");
		var params = {
			username: textfieldUserName.value,
			password: textfieldPassword.value //Ti.Utils.md5HexDigest(password.value)
		};
		loginReq.send(params);
		viewLogin.setVisible(false);
	}
	else
	{
		alert("Username/Password are required");
	}
});

/*
buttonLogin.addEventListener("click", function(e){
	//if(textfieldUserName.value == 'a' && textfieldPassword.value == 'a')
	//{
		viewLogin.setVisible(false);
		//windowProfile.setVisible(true);
		//tabGroup.add(tabProfile);
		//tabGroup.tabs[1].show();
//	}
//	else{
//		alert("Error");
//	}
});
*/