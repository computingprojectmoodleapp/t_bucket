// this sets the background color of the master UIView (when there are no windows/tab groups on it)
Titanium.UI.setBackgroundColor('#000');

// create tab group
var tabGroup = Titanium.UI.createTabGroup();

var win1 = Titanium.UI.createWindow({
	title:"test run",
	backgroundColor:"white"
});

var viewMain = Titanium.UI.createView({
	height:100,
	width:"100%",
	top:0,
	left:0,
	backgroundColor:"yellow"
});

var lblLogin = Titanium.UI.createLabel({
	text:"You have not logged in.",
	top:5, 
	left:0,
	height:50,
	width:"100%",
	textAlign:"center",
	font:{fontSize:18},
	color:"black"
});

var btnLoginPg = Titanium.UI.createButton({
	title:"Log In",
	width:"25%",
	height:"40%",
	top:50,
	left:175,
	id:"btnLogin"
});

viewMain.add(btnLoginPg);
viewMain.add(lblLogin);


var viewNews = Titanium.UI.createView({
	height:300,
	width:"100%",
	top:100,
	left:0,
	//backgroundColor:"purple"
});

var imgNews1 = Titanium.UI.createImageView({
	image:"http://courseweb.sliit.lk/pluginfile.php/10443/block_html/content/PDP.png",
	height:"15%",
	width:"25%",
	top:10,
	left:180
});

var imgNews2 = Titanium.UI.createImageView({
	image:"http://courseweb.sliit.lk/pluginfile.php/10443/block_html/content/android.png",
	height:"10%",
	width:"25%",
	top:55,
	left:180
});

var imgNews3 = Titanium.UI.createImageView({
	image:"http://courseweb.sliit.lk/pluginfile.php/10443/block_html/content/Server%202012.png",
	height:"10%",
	width:"25%",
	top:90,
	left:180
});

var imgNews4 = Titanium.UI.createImageView({
	image:"http://courseweb.sliit.lk/pluginfile.php/10443/block_html/content/Linux.png",
	height:"10%",
	width:"25%",
	top:125,
	left:180
});

var imgNews5 = Titanium.UI.createImageView({
	image:"http://courseweb.sliit.lk/pluginfile.php/10443/block_html/content/CCNA%20Routing.png",
	height:"10%",
	width:"25%",
	top:160,
	left:180
});

var imgNews6 = Titanium.UI.createImageView({
	image:"http://courseweb.sliit.lk/pluginfile.php/10443/block_html/content/cisa.png",
	height:"10%",
	width:"25%",
	top:195,
	left:180
});

var imgNews7 = Titanium.UI.createImageView({
	image:"http://courseweb.sliit.lk/pluginfile.php/10443/block_html/content/Contact.png",
	height:"20%",
	width:"25%",
	top:225,
	left:180
});

var winWeb = Titanium.UI.createWindow({
	title:"SLIIT",
	backgroundColor:"white"
});


var webViewAndroid = Titanium.UI.createWebView({
	top:0,
	left:0
});

var webViewServer = Titanium.UI.createWebView({
	top:0,
	left:0
});

imgNews2.addEventListener("click",function(e){
	webViewAndroid.url="http://www.sliit.lk/index.php/professional-programs/mobile-application-development/mobile-application-development-for-android";
	alert("You are being redirected to http://www.sliit.lk");
	winWeb.add(webViewAndroid);
	winWeb.open();
});

imgNews3.addEventListener("click",function(e){
	webViewServer.url="http://www.sliit.lk/index.php/professional-programs/installing-and-configuring-windows-server-2012-program";
	alert("You are being redirected to http://www.sliit.lk");
	winWeb.add(webViewServer);
	winWeb.open();
});

viewNews.add(imgNews1);
viewNews.add(imgNews2);
viewNews.add(imgNews3);
viewNews.add(imgNews4);
viewNews.add(imgNews5);
viewNews.add(imgNews6);
viewNews.add(imgNews7);

win1.add(viewMain);
win1.add(viewNews);


var tab1 = Titanium.UI.createTab({
	title:"Home",
	window:win1
});

var tab2 = Titanium.UI.createTab({
	title:"Login"
});

tabGroup.addTab(tab1);  
tabGroup.addTab(tab2);  


// open tab group
tabGroup.open();
